<?php

/**
 * @file
 * Views definitions for Views Global External.
 */
 
/**
 * Implements hook_views_data().
 */
function views_global_external_views_data() {
  $data['views']['global_external'] = array(
    'title' => t('External'),
    'help' => t('Pass custom text into a external display service.'),
    'field' => array(
      'handler' => 'views_handler_field_global_external',
    ),
  );
  return $data;
}