<?php

/**
 * @file
 * Definition of views_handler_field_sharethis_path.
 */

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_global_external extends views_handler_field_custom {
  function query() {
    // do nothing -- to override the parent query.
  }

  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Set the alter text field properly.
    $form['alter']['text']['#type'] = 'textfield';
  }
  
  function render($values) {
    parent::render($values);
  }
  
  function advanced_render($values) {
    parent::advanced_render($values);
    
    // Get the URL from either the custom text or the current path.
    if (strlen($this->last_render)) {
      $path = $this->last_render;
    }
    else {
      $path = current_path();
    }
    $url = url($path, array(
      'absolute' => TRUE,
    ));
    
    $title = 'Shared Item';
    if (isset($values->node_title)) {
      $title .= ': ' . $values->node_title;
    }
    
    $this->last_render = theme('sharethis', array(
      'data_options' => sharethis_get_options_array(),
      'm_title' => $title,
      'm_path' => $url,
    ));
    $this->original_value = $this->last_render;
    return $this->last_render;
  }
}
